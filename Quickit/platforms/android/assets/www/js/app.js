// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova', 'pascalprecht.translate'])
.run(function($rootScope,$ionicPlatform,$localStorage,$http, $translate) {
	$rootScope.host  = 'http://tapper.co.il/china/mobile-php/';
	//$rootScope.host  = 'http://64.74.12.34/~tapper/php/';
	$rootScope.productId = 0;
	$rootScope.custumerid = 0;
	$rootScope.isSettings = false;
	$rootScope.defaultMarket = 'YIWU FUTIEN';
	//$rootScope.marketId = 0;	
	$rootScope.marketId = -1;	
	$rootScope.productIndex = 0;
	$rootScope.savedFields = '';
	$rootScope.defaultLanguage = 'en';
	$rootScope.defaultCustumer = 'My Self';
	$rootScope.defaultCustumerIndex = '0';
//	$rootScope.host  = 'http://localhost/chinabuy/php/';


	$ionicPlatform.registerBackButtonAction(function (event) 
	{
	 confirmbox = confirm("do you want to close the app?");
	 if (confirmbox)
	 {
      navigator.app.exitApp();
	 }
	 else
	 {
		 event.preventDefault();
	 }
    }
	,100);
	
		
	$ionicPlatform.ready(function() {
	
	/*
	if(typeof navigator.globalization !== "undefined") {
		navigator.globalization.getPreferredLanguage(function(language) {
			$translate.use((language.value).split("-")[0]).then(function(data) {
				console.log("SUCCESS -> " + data);
			}, function(error) {
				console.log("ERROR -> " + error);
			});
		}, null);
	}
	*/

	
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }


	
  });
  

})

  
.config(function($stateProvider, $urlRouterProvider, $translateProvider) {
	
  // configures staticFilesLoader
  $translateProvider.useStaticFilesLoader({
    prefix: 'js/data/locale-',
    suffix: '.json'
  });
  // load 'en' table on startup
  $translateProvider.preferredLanguage('en');
  $translateProvider.fallbackLanguage('en');

  
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
   .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainController'
      }
    }
  })
 
   .state('app.languages', {
    url: '/languages',
    views: {
      'menuContent': {
        templateUrl: 'templates/languages.html',
        controller: 'LanguagesCtrl'
      }
    }
  })

  
   .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
        controller: 'RegisterController'
      }
    }
  }) 
  
   .state('app.forgot', {
    url: '/forgot',
    views: {
      'menuContent': {
        templateUrl: 'templates/forgot.html',
        controller: 'ForgotPasswordCtrl'
      }
    }
  })   
  
 /*
    .state('app.dashboard', {
    url: '/dashboard/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/dashboard.html',
        controller: 'DashBoard'
      }
    }
  })
  
  
      .state('app.addcatagory', {
    url: '/addcatagory/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/add_catagory.html',
        controller: 'AddCatagory'
      }
    }
  })
  */
    .state('app.products', {
    url: '/products/',
    views: {
      'menuContent': {
        templateUrl: 'templates/products.html',
        controller: 'Products'
      }
    }
  })
  

  
   .state('app.welcome', {
    url: '/welcome/',
    views: {
      'menuContent': {
        templateUrl: 'templates/welcome.html',
        controller: 'welcomeCtrl'
      }
    }
  })
  
     .state('app.addproduct', {
    url: '/addproduct/:productIndex',
    views: {
      'menuContent': {
        templateUrl: 'templates/add_product.html',
        controller: 'addproductCtrl'
      }
    }
  })
  /*
       .state('app.prevproduct', {
    url: '/prevproduct/:id/:searchitem/:catagoryid',
    views: {
      'menuContent': {
        templateUrl: 'templates/prev_product.html',
        controller: 'prevproductCtrl'
      }
    }
  })
  */
     .state('app.quotes', {
    url: '/quotes',
    views: {
      'menuContent': {
        templateUrl: 'templates/quotes.html',
        controller: 'quotesCtrl'
      }
    }
  })
  
    .state('app.editquote', {
    url: '/editquote/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/quotes_manage.html',
        controller: 'quotesEditCtrl'
      }
    }
  })
  /*
   .state('app.catProducts', {
    url: '/catProducts/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/prev_product.html',
        controller: 'prevproductCtrl'
      }
    }
  })
  */
      .state('app.customers', {
    url: '/customers/',
    views: {
      'menuContent': {
        templateUrl: 'templates/customers.html',
        controller: 'customersCtrl'
      }
    }
  })
  
        .state('app.custumermanage', {
    url: '/custumermanage/:id/',
    views: {
      'menuContent': {
        templateUrl: 'templates/customers_manage.html',
        controller: 'customermanageCtrl'
      }
    }
  })
  
  .state('app.catagory', {
    url: '/catagory/:id/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/catagory.html',
        controller: 'catagoryCtrl'
      }
    }
  })
  
    .state('app.markets', {
    url: '/markets/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/markets.html',
        controller: 'MarketsCtrl'
      }
    }
  })
  
      .state('app.other', {
    url: '/other/',
    views: {
      'menuContent': {
        templateUrl: 'templates/other.html',
        controller: 'OtherCtrl'
      }
    }
  })

   .state('app.supplier', {
    url: '/supplier/',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplier.html',
        controller: 'SupplierCtrl'
      }
    }
  })

   .state('app.productsexcel', {
    url: '/productsexcel/',
    views: {
      'menuContent': {
        templateUrl: 'templates/products_excel.html',
        controller: 'Products'
      }
    }
  })
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/languages');
});


